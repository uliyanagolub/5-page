import Matrixes.DiagMatrix;
import Matrixes.EmptyMatrix;
import Matrixes.Matrix;
import Matrixes.UpTriangleMatrix;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMatrixes {
    @Test
    public void testDet() throws Exception {
        EmptyMatrix matrix = new Matrix(1);
        matrix.changeElement(0,0,1);
        assertEquals(1,matrix.getElement(0,0), 1e-10);
        assertEquals(1,matrix.getDeterminant(),1e-10);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testDiagMatrix1() throws Exception{
        EmptyMatrix matrix = new DiagMatrix(2);
        matrix.changeElement(0,1,1);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testDiagMatrix2() throws Exception{
        EmptyMatrix matrix = new DiagMatrix(2);
        assertEquals("It is a diagonal matrix",matrix.getElement(1,0));
    }
    @Test(expected = IllegalArgumentException.class)
    public void testUpTriangleMatrix1() throws Exception{
        EmptyMatrix matrix = new UpTriangleMatrix(2);
        matrix.changeElement(0,1,1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testUpTriangleMatrix2() throws Exception{
        EmptyMatrix matrix = new UpTriangleMatrix(2);
        assertEquals("It is an up-triangle matrix",matrix.getElement(0,1));
    }
    @Test
    public void testUpTriangleMatrix() throws Exception{
        EmptyMatrix matrix = new UpTriangleMatrix(2);
        matrix.changeElement(1,1,1);
        matrix.changeElement(0,0,1);
        assertEquals(1,matrix.getDeterminant(),1e-10);
    }

    @Test
    public void testDiagDet() throws Exception {
        EmptyMatrix matrix = new DiagMatrix(2);
        matrix.fillRand();
        matrix.changeElement(0, 0, 1);
        matrix.changeElement(1, 1, 2);
        assertEquals(2, matrix.getDeterminant(), 1e-10);
    }
    @Test
    public void getDet4() throws Exception {
        EmptyMatrix m = new Matrix(2);
        EmptyMatrix m2 = new Matrix(2);
        m.changeElement(0,0,5);
        m.changeElement(0,1,6);
        m.changeElement(1,0,6);
        m.changeElement(1,1,5);
        assertEquals(-11,m.getDeterminant(),1e-10);
        assertEquals(0, m2.getDeterminant(),1e-10);
    }
    @Test
    public void getDet7() throws Exception {
        EmptyMatrix m = new Matrix(3);
        m.changeElement(0,0,1);
        m.changeElement(0,1,2);
        m.changeElement(0,2,3);
        m.changeElement(1,0,4);
        m.changeElement(1,1,5);
        m.changeElement(1,2,6);
        m.changeElement(2,0,45);
        m.changeElement(2,1,-5);
        m.changeElement(2,2,11);

        assertEquals(-198,m.getDeterminant(),1e-10);
    }

}
