import MatrixService.MatrixDetComparator;
import Matrixes.DiagMatrix;
import Matrixes.IMatrix;
import Matrixes.Matrix;
import Matrixes.UpTriangleMatrix;
import org.junit.Before;
import org.junit.Test;

import static MatrixService.MatrixService.arrangeMatrices;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

public class TestMatrixService {
    private IMatrix m1;
    private IMatrix m2;
    private IMatrix m3 = new Matrix(1);
    private IMatrix m4 = new Matrix(1);


    @Before
    public void setUp() {
         m1 = new Matrix(5).fillRand();
         m2 = new Matrix(5).fillRand();

    }

    @Test
    public void comparatorDemo() {
        int res = new MatrixDetComparator().compare(m1, m2);
        assertTrue(
                (m1.getDeterminant() < m2.getDeterminant() && res < 0) ^
                        (m1.getDeterminant() > m2.getDeterminant() && res > 0) ^
                        (Double.compare(m1.getDeterminant(), m2.getDeterminant()) == 0 && res == 0)
        );
    }

    @Test
    public void comparatorTest() {
        Matrix matrix = new Matrix(1,2,3,4);   // -2
        DiagMatrix diagMatrix = new DiagMatrix(4, 0.25, 7, 2, -1); // -14
        DiagMatrix otherDiag = new DiagMatrix(4, 7, 8); // >200
        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(3,1,2,3,4,5,6); //24

        IMatrix[] actual = arrangeMatrices(upTriangleMatrix, diagMatrix, matrix, otherDiag);
        IMatrix[] expected = new IMatrix[]{
                diagMatrix,
                matrix,
                upTriangleMatrix,
                otherDiag
        };
        assertArrayEquals(expected, actual);
//        Assertions.assertEquals(0, new MatrixDetComparator().compare(m3, m4));
//        Assertions.assertEquals(-1, new MatrixDetComparator().compare(m3, m4));
//        Assertions.assertEquals(1, new MatrixDetComparator().compare(m4, m3));
//
    }
}