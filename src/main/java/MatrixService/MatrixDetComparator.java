package MatrixService;

import Matrixes.IMatrix;

import java.util.Comparator;

//е компаратор для матриц, который сравнивает определители матриц

public class MatrixDetComparator implements Comparator<IMatrix> {

    @Override
    public int compare(IMatrix m1, IMatrix m2) {
        return Double.compare(m1.getDeterminant(),m2.getDeterminant());
    }
}
