package MatrixService;

import Matrixes.IMatrix;
import Matrixes.Matrix;

import java.util.Arrays;

//класс MatrixService
public class MatrixService {

/* статический метод arrangeMatrices, который получает на вход массив матриц произвольного
вида и сортирует этот массив по неубыванию определителей матриц*/
        public static  IMatrix[] arrangeMatrices(IMatrix... arr){
            Arrays.parallelSort(arr, new MatrixDetComparator());
            return arr;
        }
}
