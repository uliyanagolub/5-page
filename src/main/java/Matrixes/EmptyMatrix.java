package Matrixes;

import java.util.Arrays;
import java.util.Objects;

abstract public class EmptyMatrix implements IMatrix{
    protected double[] array;
    protected double determinant;
    protected boolean detFlag = false;

    public EmptyMatrix(int n){
        this.array =new double[n];
        Arrays.fill(array,0);
    }
    public EmptyMatrix(double ... array){
        this.array = array;
        determinant = 0;
    }


    public IMatrix fillRand(){
        for(int i=0; i<array.length; i++){
            array[i] = Math.random()*10;
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmptyMatrix)) return false;
        EmptyMatrix that = (EmptyMatrix) o;
        return Double.compare(that.getDeterminant(), getDeterminant()) == 0 &&
                detFlag == that.detFlag &&
                Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getDeterminant(), detFlag);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }
}
