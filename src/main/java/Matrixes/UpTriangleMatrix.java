package Matrixes;

//класс UpTriangleMatrix (верхнетреугольная матрица)

public class UpTriangleMatrix extends EmptyMatrix implements IMatrix {
    private int length;
    public UpTriangleMatrix(int n) {
        super((n*n + n)/2);
        length = n;
    }

    public UpTriangleMatrix(int len, double... array) {
        super(array);
        if (array.length < (len*len + len)/2) {
            throw new IllegalArgumentException("Not enough elements");
        }
        length = len;
    }

// получить элемент с заданными индексами
    @Override
    public double getElement(int x, int y){
        if (x<y){
          return 0;
        }
        if (x*(length - x)+y <0 || x*(length - x)+y > array.length) {
            System.out.println( "x=" + x + " , "  + "y=" + y);
        }
        return array[x*(length - x)+y];
    }


// изменить элемент с заданными индексами
    @Override
    public void changeElement(int x, int y, double element){
        if (x<y){
            throw new IllegalArgumentException("It is an up-triangle matrix");
        }
        array[x*(length - x)+y]= element;
        detFlag = false;
    }

//вычислить определитель матрицы
    @Override
    public double getDeterminant(){
        if (detFlag) {
            return determinant;
        }
        determinant = 1;
        for (int i = 0; i < getLength(); i++) {
            determinant = determinant * getElement(i, i);
        }
        detFlag = true;
        return determinant;
    }
// длинна матрицы
    @Override
    public int getLength(){
        return this.length;
    }
}
