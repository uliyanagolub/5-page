package Matrixes;

//Напишите класс Matrix (квадратная матрица произвольного вида).

public class Matrix extends EmptyMatrix implements IMatrix {

    public Matrix(int n) { // конструктор по размерности
        super(n*n);
    }
    public Matrix(double... array) { // конструктор по элементам диагонали
        super(array);
    }

    @Override
    public int getLength(){
        return (int) Math.sqrt(array.length);
    }
    
    // получить элемент с заданными индексами
    @Override
    public double getElement(int x, int y){
        return array[x*getLength()+y];
    }

    //изменить элемент с заданными индексами
    @Override
    public void changeElement(int x, int y, double element) {
        this.array[x*getLength()+y] = element;
        detFlag = false;
    }
    
    // поменять местами строки
    public void changeRows(int i1, int i2, Matrix matrix){
        for(int i = 0; i <matrix.getLength(); i++){
            double tmp = matrix.array[i1*getLength()+i];
            matrix.array[i1*getLength()+i] = matrix.array[i2*getLength()+i];
            matrix.array[i2*getLength()+i] = tmp;
        }
    }
    
    //конструктор копирования
    public Matrix copy() {
        Matrix newMatrix = new Matrix(getLength());
        newMatrix.array = array.clone();
        return newMatrix;
    }
    
    //вычислить определитель матрицы
    private double calculateDet(int index, Matrix m){
        double sign = 1;
        //if(index + 1 == m.getLength()){
           // return m.getElement(index,index);
     //   }
        for (int i= index; i+1 < m.getLength() && m.getElement(index,index)==0; i++){
            if (m.getElement(i+1,index)==0){
                continue;
            }
            changeRows(i+1,index,m);
            sign = sign*(-1);
        }
        if (m.getElement(index,index)==0){
            return 0;
        }
        for (int i = index; i < m.getLength(); i++){
            if (m.getElement(i,index)!=0){
                double div = m.getElement(i, index);
                sign *= div;
                for (int j = index; j < m.getLength(); ++j) {
                    m.changeElement(i, j, (m.getElement(i, j) / div));
                }
            }
        }
        for(int i = index + 1;i < m.getLength();++i){
            if(m.getElement(i,index) != 0) {
                for (int j = index; j < m.getLength(); ++j) {
                    m.changeElement(i, j, m.getElement(i, j) - m.getElement(index, j));
                }
            }
        }
        return sign;
    }
    
    // поле для хранения вычисленного значения определителя
    @Override
    public double getDeterminant() {
        if (detFlag) {
            return determinant;
        }
        determinant = 1;
        Matrix tmp = this.copy();
        for (int index = 0; index < getLength(); index++) {
            determinant = determinant * calculateDet(index, tmp);
        }
        detFlag = true;
        return determinant;
    }

}
