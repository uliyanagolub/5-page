package Matrixes;

// интерфейс IMatrix (квадратная матрица вещественных чисел)

public interface IMatrix {
    double getElement(int x, int y); // получить элемент с заданными индексами
    void changeElement(int x, int y, double element); //изменить элемент с заданными индексами
    double getDeterminant();// вычислить определитель матрицы
    int getLength();

}
