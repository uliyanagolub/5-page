package Matrixes;

/* производный класс DiagMatrix (диагональная матрица) */

public class DiagMatrix extends  EmptyMatrix implements IMatrix {

    public DiagMatrix(int n) {
        super(n);
    }

/* Создайте конструктор по размерности матрицы и конструктор по набору 
    элементов на диагонали.*/
    
    public DiagMatrix(double... array) { 
        super(array);
    }
    @Override
    public double getElement(int x, int y){
        if (x!=y){
            return 0;
        }
        return array[x];
    }
    
    /* Метод изменения элемента при попытке записать ненулевое значение вне диагонали 
    должен выбрасывать исключение*/
    
    @Override
    public void changeElement(int x, int y, double element){
        if (x!=y){
            throw new IllegalArgumentException("It is a diagonal matrix");
        }
        array[x] = element;
        detFlag = false;
    }
    @Override
    public double getDeterminant(){
        if (detFlag) {
            return determinant;
        }
        determinant = 1;
        for (int i = 0; i < getLength(); i++) {
            determinant = determinant * array[i];
        }
        detFlag = true;
        return determinant;
    }
    @Override
    public int getLength(){
        return array.length;
    }

}
